package com.blubito.scheduler.domain.enumeration;

/**
 * The Status enumeration.
 */
public enum Status {
    APPROVED, DECLINED, NEW
}
