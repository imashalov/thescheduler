package com.blubito.scheduler.domain.enumeration;

/**
 * The RequestType enumeration.
 */
public enum RequestType {
    HOLIDAY, SICK_LEAVE, MATERNITY, PATERNITY
}
