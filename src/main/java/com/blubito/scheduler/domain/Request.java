package com.blubito.scheduler.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import com.blubito.scheduler.domain.enumeration.RequestType;

import com.blubito.scheduler.domain.enumeration.Status;

/**
 * A Request.
 */
@Entity
@Table(name = "request")
public class Request extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "jhi_type", nullable = false)
    private RequestType type;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private Status status;

    @NotNull
    @Column(name = "jhi_from", nullable = false)
    private ZonedDateTime from;

    @NotNull
    @Column(name = "jhi_to", nullable = false)
    private ZonedDateTime to;

    //comma separated list of emails
    @Size(max = 500)
    @Column(name = "interested_parties", length = 500)
    private String interestedParties;

    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "reason", length = 500, nullable = false)
    private String reason;

    @ManyToOne
    @JsonIgnoreProperties("requests")
    private User user;

    @ManyToOne
    @JsonIgnoreProperties("requests")
    private User admin;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public RequestType getType() {
        return type;
    }

    public Request type(RequestType type) {
        this.type = type;
        return this;
    }

    public void setType(RequestType type) {
        this.type = type;
    }

    public Status getStatus() {
        return status;
    }

    public Request status(Status status) {
        this.status = status;
        return this;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public ZonedDateTime getFrom() {
        return from;
    }

    public Request from(ZonedDateTime from) {
        this.from = from;
        return this;
    }

    public void setFrom(ZonedDateTime from) {
        this.from = from;
    }

    public ZonedDateTime getTo() {
        return to;
    }

    public Request to(ZonedDateTime to) {
        this.to = to;
        return this;
    }

    public void setTo(ZonedDateTime to) {
        this.to = to;
    }

    public String getInterestedParties() {
        return interestedParties;
    }

    public Request interestedParties(String interestedParties) {
        this.interestedParties = interestedParties;
        return this;
    }

    public void setInterestedParties(String interestedParties) {
        this.interestedParties = interestedParties;
    }

    public String getReason() {
        return reason;
    }

    public Request reason(String reason) {
        this.reason = reason;
        return this;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public User getUser() {
        return user;
    }

    public Request user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getAdmin() {
        return admin;
    }

    public Request admin(User user) {
        this.admin = user;
        return this;
    }

    public void setAdmin(User user) {
        this.admin = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Request)) {
            return false;
        }
        return id != null && id.equals(((Request) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Request{" +
            "id=" + getId() +
            ", type='" + getType() + "'" +
            ", status='" + getStatus() + "'" +
            ", from='" + getFrom() + "'" +
            ", to='" + getTo() + "'" +
            ", interestedParties='" + getInterestedParties() + "'" +
            ", reason='" + getReason() + "'" +
            "}";
    }
}
