/**
 * Data Access Objects used by WebSocket services.
 */
package com.blubito.scheduler.web.websocket.dto;
