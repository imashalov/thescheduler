/**
 * View Models used by Spring MVC REST controllers.
 */
package com.blubito.scheduler.web.rest.vm;
