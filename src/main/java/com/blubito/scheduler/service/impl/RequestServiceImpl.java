package com.blubito.scheduler.service.impl;

import com.blubito.scheduler.domain.User;
import com.blubito.scheduler.domain.enumeration.Status;
import com.blubito.scheduler.security.SecurityUtils;
import com.blubito.scheduler.service.RequestService;
import com.blubito.scheduler.domain.Request;
import com.blubito.scheduler.repository.RequestRepository;
import com.blubito.scheduler.service.UserService;
import com.blubito.scheduler.service.dto.RequestDTO;
import com.blubito.scheduler.service.mapper.RequestMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Request}.
 */
@Service
@Transactional
public class RequestServiceImpl implements RequestService {

    private final Logger log = LoggerFactory.getLogger(RequestServiceImpl.class);

    private final RequestRepository requestRepository;

    private final RequestMapper requestMapper;
    private final UserService userService;

    public RequestServiceImpl(RequestRepository requestRepository, RequestMapper requestMapper, UserService userService) {
        this.requestRepository = requestRepository;
        this.requestMapper = requestMapper;
        this.userService = userService;
    }

    /**
     * Save a request.
     *
     * @param requestDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public RequestDTO save(RequestDTO requestDTO) {
        log.debug("Request to save Request : {}", requestDTO);
        //must be refactored
        User admin =userService.getUserWithAuthoritiesByLogin("admin").get();
        User user = userService.getUserWithAuthoritiesByLogin(SecurityUtils.getCurrentUserLogin().get()).get();

        requestDTO.setAdminId(admin.getId());
        requestDTO.setUserId(user.getId());
        if(requestDTO.getId() == null){
            requestDTO.setStatus(Status.NEW);
        }
        //
        Request request = requestMapper.toEntity(requestDTO);

        request = requestRepository.save(request);

        /*send email to admin
        .
        .
        .
        */

        /*send confirmation email to user
        .
        .
        .
        */
        /*send confirmation emails to interested parties list
        .
        .
        .
        */
        return requestMapper.toDto(request);


    }

    /**
     * Get all the requests.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<RequestDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Requests");
        return requestRepository.findAll(pageable)
            .map(requestMapper::toDto);
    }


    /**
     * Get one request by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<RequestDTO> findOne(Long id) {
        log.debug("Request to get Request : {}", id);
        return requestRepository.findById(id)
            .map(requestMapper::toDto);
    }

    /**
     * Delete the request by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Request : {}", id);
        requestRepository.deleteById(id);
    }

    @Override
    public Page<RequestDTO> getCurrentUserRequests(Pageable pageable) {
        return requestRepository.findByUserIsCurrentUserPageable(pageable).map(requestMapper::toDto);
    }

    @Override
    public void approve(Long id) {
        requestRepository.findById(id).get().setStatus(Status.APPROVED);
    }
    @Override
    public void decline(Long id) {
        requestRepository.findById(id).get().setStatus(Status.DECLINED);
    }
}
