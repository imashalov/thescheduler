package com.blubito.scheduler.service.dto;
import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import com.blubito.scheduler.domain.enumeration.RequestType;
import com.blubito.scheduler.domain.enumeration.Status;

/**
 * A DTO for the {@link com.blubito.scheduler.domain.Request} entity.
 */
public class RequestDTO implements Serializable {

    private Long id;

    @NotNull
    private RequestType type;

    private Status status;

    @NotNull
    private ZonedDateTime from;

    @NotNull
    private ZonedDateTime to;

    @Size(max = 500)
    private String interestedParties;

    @NotNull
    @Size(min = 1, max = 500)
    private String reason;

    private Long userId;

    private String userEmail;

    private Long adminId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public RequestType getType() {
        return type;
    }

    public void setType(RequestType type) {
        this.type = type;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public ZonedDateTime getFrom() {
        return from;
    }

    public void setFrom(ZonedDateTime from) {
        this.from = from;
    }

    public ZonedDateTime getTo() {
        return to;
    }

    public void setTo(ZonedDateTime to) {
        this.to = to;
    }

    public String getInterestedParties() {
        return interestedParties;
    }

    public void setInterestedParties(String interestedParties) {
        this.interestedParties = interestedParties;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public Long getAdminId() {
        return adminId;
    }

    public void setAdminId(Long userId) {
        this.adminId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RequestDTO requestDTO = (RequestDTO) o;
        if (requestDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), requestDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RequestDTO{" +
            "id=" + id +
            ", type=" + type +
            ", status=" + status +
            ", from=" + from +
            ", to=" + to +
            ", interestedParties='" + interestedParties + '\'' +
            ", reason='" + reason + '\'' +
            ", userId=" + userId +
            ", userEmail='" + userEmail + '\'' +
            ", adminId=" + adminId +
            '}';
    }
}
