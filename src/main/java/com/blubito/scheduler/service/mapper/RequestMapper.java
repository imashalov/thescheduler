package com.blubito.scheduler.service.mapper;

import com.blubito.scheduler.domain.*;
import com.blubito.scheduler.service.dto.RequestDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Request} and its DTO {@link RequestDTO}.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface RequestMapper extends EntityMapper<RequestDTO, Request> {

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "user.email", target = "userEmail")

    @Mapping(source = "admin.id", target = "adminId")
    RequestDTO toDto(Request request);

    @Mapping(source = "userId" , target = "user")
    @Mapping(source = "adminId", target = "admin")
    Request toEntity(RequestDTO requestDTO);

    default Request fromId(Long id) {
        if (id == null) {
            return null;
        }
        Request request = new Request();
        request.setId(id);
        return request;
    }
}
