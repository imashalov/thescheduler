package com.blubito.scheduler.repository;

import com.blubito.scheduler.domain.Request;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

 import java.util.List;

/**
 * Spring Data  repository for the Request entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RequestRepository extends JpaRepository<Request, Long> {

    @Query("select request from Request request where request.user.login = ?#{principal.username}")
    List<Request> findByUserIsCurrentUser();

    @Query("select request from Request request where request.admin.login = ?#{principal.username}")
    List<Request> findByAdminIsCurrentUser();

    @Query("select request from Request request where request.user.login = ?#{principal.username}")
    Page<Request> findByUserIsCurrentUserPageable(Pageable pageable);

}
