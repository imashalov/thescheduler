import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { TheschedulerSharedModule } from 'app/shared';
import {
  RequestComponent,
  RequestDetailComponent,
  RequestUpdateComponent,
  RequestDeletePopupComponent,
  RequestDeleteDialogComponent,
  requestRoute,
  requestPopupRoute
} from './';
import { RequestApproveDialogComponent, RequestApprovePopupComponent } from './request-approve-dialog.component';
import { RequestDeclineDialogComponent, RequestDeclinePopupComponent } from './request-decline-dialog.component';

const ENTITY_STATES = [...requestRoute, ...requestPopupRoute];

@NgModule({
  imports: [TheschedulerSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    RequestComponent,
    RequestDetailComponent,
    RequestUpdateComponent,
    RequestDeleteDialogComponent,
    RequestDeletePopupComponent,
    RequestApproveDialogComponent,
    RequestApprovePopupComponent,
    RequestDeclineDialogComponent,
    RequestDeclinePopupComponent
  ],
  entryComponents: [
    RequestComponent,
    RequestUpdateComponent,
    RequestDeleteDialogComponent,
    RequestDeletePopupComponent,
    RequestApproveDialogComponent,
    RequestApprovePopupComponent,
    RequestDeclineDialogComponent,
    RequestDeclinePopupComponent,
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TheschedulerRequestModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
