import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IRequest } from 'app/shared/model/request.model';

type EntityResponseType = HttpResponse<IRequest>;
type EntityArrayResponseType = HttpResponse<IRequest[]>;

@Injectable({ providedIn: 'root' })
export class RequestService {

  public resourceUrl = SERVER_API_URL + 'api/requests';
  public resourceUrlApprove = SERVER_API_URL + 'api/requests-approve';
  public resourceUrlDecline = SERVER_API_URL + 'api/requests-decline';
  public resourceUrlCurrentUser = SERVER_API_URL + 'api/requests-current-user';

  constructor(protected http: HttpClient) {}

  create(request: IRequest): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(request);
    return this.http
      .post<IRequest>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(request: IRequest): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(request);
    return this.http
      .put<IRequest>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IRequest>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IRequest[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  getCurrentUserRequests(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IRequest[]>(this.resourceUrlCurrentUser, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
  approve(id: number) {
    return this.http.post<any>(`${this.resourceUrlApprove}/${id}`, { observe: 'response' });
  }
  decline(id: number) {
    return this.http.post<any>(`${this.resourceUrlDecline}/${id}`, { observe: 'response' });
  }
  protected convertDateFromClient(request: IRequest): IRequest {
    const copy: IRequest = Object.assign({}, request, {
      from: request.from != null && request.from.isValid() ? request.from.toJSON() : null,
      to: request.to != null && request.to.isValid() ? request.to.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.from = res.body.from != null ? moment(res.body.from) : null;
      res.body.to = res.body.to != null ? moment(res.body.to) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((request: IRequest) => {
        request.from = request.from != null ? moment(request.from) : null;
        request.to = request.to != null ? moment(request.to) : null;
      });
    }
    return res;
  }
}
