import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IRequest } from 'app/shared/model/request.model';
import { RequestService } from './request.service';

@Component({
  selector: 'jhi-request-approve-dialog',
  templateUrl: './request-approve-dialog.component.html'
})
export class RequestApproveDialogComponent {
  request: IRequest;

  constructor(protected requestService: RequestService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmApprove(id: number) {
    this.requestService.approve(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'requestListModification',
        content: 'Approve a request'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-request-approve-popup',
  template: ''
})
export class RequestApprovePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ request }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(RequestApproveDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.request = request;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/request', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/request', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
