import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IRequest, Request, RequestType } from 'app/shared/model/request.model';
import { RequestService } from './request.service';
import { IUser, UserService, AccountService } from 'app/core';

@Component({
  selector: 'jhi-request-update',
  templateUrl: './request-update.component.html'
})
export class RequestUpdateComponent implements OnInit {
  request: IRequest;
  isSaving: boolean;
  currentAccount: any;

  editForm = this.fb.group({
    id: [],
    type: [null, [Validators.required]],
    status: [null],
    from: [null, [Validators.required]],
    to: [null, [Validators.required]],
    interestedParties: [null, [Validators.maxLength(500)]],
    reason: [null, [Validators.required, Validators.minLength(1), Validators.maxLength(500)]],
    userId: [],
    adminId: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected requestService: RequestService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    protected accountService: AccountService,
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ request }) => {
      this.updateForm(request);
      this.request = request;
    });
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });

    // Set holiday as default request type
    if (!this.request.id) {
      this.request.type = RequestType.HOLIDAY;
      this.updateForm(this.request);
    }
    // this.userService
    //   .query()
    //   .pipe(
    //     filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
    //     map((response: HttpResponse<IUser[]>) => response.body)
    //   )
    //   .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(request: IRequest) {
    this.editForm.patchValue({
      id: request.id,
      type: request.type,
      status: request.status,
      from: request.from != null ? request.from.format(DATE_TIME_FORMAT) : null,
      to: request.to != null ? request.to.format(DATE_TIME_FORMAT) : null,
      interestedParties: request.interestedParties,
      reason: request.reason,
      userId: request.userId,
      adminId: request.adminId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const request = this.createFromForm();
    if (request.id !== undefined) {
      this.subscribeToSaveResponse(this.requestService.update(request));
    } else {
      this.subscribeToSaveResponse(this.requestService.create(request));
    }
  }

  private createFromForm(): IRequest {
    const entity = {
      ...new Request(),
      id: this.editForm.get(['id']).value,
      type: this.editForm.get(['type']).value,
      status: this.editForm.get(['status']).value,
      from: this.editForm.get(['from']).value != null ? moment(this.editForm.get(['from']).value, DATE_TIME_FORMAT) : undefined,
      to: this.editForm.get(['to']).value != null ? moment(this.editForm.get(['to']).value, DATE_TIME_FORMAT) : undefined,
      interestedParties: this.editForm.get(['interestedParties']).value,
      reason: this.editForm.get(['reason']).value,
      userId: this.editForm.get(['userId']).value,
      adminId: this.editForm.get(['adminId']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRequest>>) {
    result.subscribe((res: HttpResponse<IRequest>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }
}
