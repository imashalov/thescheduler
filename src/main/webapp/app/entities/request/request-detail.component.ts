import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IRequest } from 'app/shared/model/request.model';
import { AccountService } from 'app/core/auth/account.service';

@Component({
  selector: 'jhi-request-detail',
  templateUrl: './request-detail.component.html'
})
export class RequestDetailComponent implements OnInit {
  request: IRequest;
  currentAccount: any;

  constructor(
    protected activatedRoute: ActivatedRoute,
    protected accountService: AccountService, ) { }

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ request }) => {
      this.request = request;
    });
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });

  }

  previousState() {
    window.history.back();
  }
}
