import { Moment } from 'moment';

export const enum RequestType {
  HOLIDAY = 'HOLIDAY',
  SICK_LEAVE = 'SICK_LEAVE',
  MATERNITY = 'MATERNITY',
  PATERNITY = 'PATERNITY'
}

export const enum Status {
  APPROVED = 'APPROVED',
  DECLINED = 'DECLINED',
  NEW = 'NEW'
}

export interface IRequest {
  id?: number;
  type?: RequestType;
  status?: Status;
  from?: Moment;
  to?: Moment;
  interestedParties?: string;
  reason?: string;
  userId?: number;
  userEmail?: string;
  adminId?: number;
}

export class Request implements IRequest {
  constructor(
    public id?: number,
    public type?: RequestType,
    public status?: Status,
    public from?: Moment,
    public to?: Moment,
    public interestedParties?: string,
    public reason?: string,
    public userId?: number,
    public userEmail?:string,
    public adminId?: number
  ) {}
}
