import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TheschedulerSharedLibsModule, TheschedulerSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [TheschedulerSharedLibsModule, TheschedulerSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [TheschedulerSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TheschedulerSharedModule {
  static forRoot() {
    return {
      ngModule: TheschedulerSharedModule
    };
  }
}
